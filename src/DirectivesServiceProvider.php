<?php

namespace WwwDot\BladeDirectives;

use Illuminate\Support\{Str, ServiceProvider, Facades\Blade};

class DirectivesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        collect(get_class_methods($this))->map(function ($methodName) {
            if (Str::startsWith($methodName, 'directive')) {
                $directiveName = str_replace('directive', null, $methodName);

                Blade::directive(strtolower($directiveName), function ($expression) use ($methodName){
                    return $this->{$methodName}($expression);
                });
            }
        });
    }

    /**
     * Parse expression.
     *
     * @param  string $expression
     * @return \Illuminate\Support\Collection
     */
    private function parseMultipleArgs($expression)
    {
        return collect(explode(',', $expression))->map(function ($item) {
            return trim($item);
        });
    }

    /**
     * Strip single quotes.
     *
     * @param  string $expression
     * @return string
     */
    private function stripQuotes($expression)
    {
        return str_replace(["'", "\"",], null, $expression);
    }

    /**
     * Register meta directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveMeta($expression = null)
    {
        $expression = $this->parseMultipleArgs($expression)->map(function ($item) {
            return $this->stripQuotes($item);
        });

        return "<meta name=\"{$expression->get(0)}\" content=\"{$expression->get(1)}\">";
    }

    /**
     * Register route directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveRoute($expression)
    {
        return "<?php echo route({$expression});";
    }

    /**
     * Register title directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveTitle($expression) {
        $expression = $this->stripQuotes($expression);
        return "<?php \$__env->startSection('title', '{$expression} - '); ?>";
    }

    /**
     * Register glyph directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveGlyph($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"glyphicons glyphicons-{$expression}\"></i>";
    }

    /**
     * Register mdi directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveMdi($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"mdi mdi-{$expression}\"></i>";
    }

    /**
     * Register fa directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveFa($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"fa fa-{$expression}\"></i>";
    }

    /**
     * Register fas directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveFas($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"fas fa-{$expression}\"></i>";
    }

    /**
     * Register far directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveFar($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"far fa-{$expression}\"></i>";
    }

    /**
     * Register fal directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveFal($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"fal fa-{$expression}\"></i>";
    }

    /**
     * Register fab directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveFab($expression)
    {
        $expression = $this->stripQuotes($expression);

        return "<i class=\"fab fa-{$expression}\"></i>";
    }

    /**
     * Register repeat directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveR($expression)
    {
        return "<?php for (\$iteration = 0 ; \$iteration < (int) {$expression}; \$iteration++): ?>";
    }

    /**
     * Register endrepeat directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndrepeat($expression)
    {
        return '<?php endfor; ?>';
    }

    /**
     * Register pushonce directive.
     *
     * @param  $expression
     * @return string
     */
    private function directivePushonce($expression)
    {
        list($pushName, $pushSub) = explode(':', trim(substr($expression, 1, -1)));

        $key = '__pushonce_'.str_replace('-', '_', $pushName).'_'.str_replace('-', '_', $pushSub);

        return "<?php if(! isset(\$__env->{$key})): \$__env->{$key} = 1; \$__env->startPush('{$pushName}'); ?>";
    }

    /**
     * Register endpushonce directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndpushonce($expression)
    {
        return '<?php $__env->stopPush(); endif; ?>';
    }

    /**
     * Register typeof directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveTypeof($expression)
    {
        $expression = $this->parseMultipleArgs($expression);

        return  "<?php if (gettype({$expression->get(0)}) == {$expression->get(1)}) : ?>";
    }

    /**
     * Register endtypeof directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndtypeof($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register instanceof directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveInstanceof($expression)
    {
        $expression = $this->parseMultipleArgs($expression);

        return  "<?php if ({$expression->get(0)} instanceof {$expression->get(1)}) : ?>";
    }

    /**
     * Register endinstanceof directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndinstanceof($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register routeisnot directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveRouteisnot($expression)
    {
        return "<?php if (! fnmatch({$expression}, Route::currentRouteName())) : ?>";
    }

    /**
     * Register endrouteisnot directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndrouteisnot($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register routeis directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveRouteis($expression)
    {
        return "<?php if (fnmatch({$expression}, Route::currentRouteName())) : ?>";
    }

    /**
     * Register endrouteis directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndrouteis($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register inline directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveInline($expression)
    {
        $include = "/* {$expression} */\n <?php include ({$expression}); ?>\n";

        if (ends_with($expression, ".html'")) {
            return $include;
        }

        if (ends_with($expression, ".css'")) {
            return "<style>\n {$include}</style>";
        }

        if (ends_with($expression, ".js'")) {
            return "<script>\n {$include}</script>";
        }

        return $include;
    }

    /**
     * Register script directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveScript($expression)
    {
        if (! empty($expression)) {
            $expression = $this->stripQuotes($expression);

            return "<script src=\"{$expression}\"></script>";
        }

        return '<script>';
    }

    /**
     * Register endscript directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndscript($expression)
    {
        return '</script>';
    }

    /**
     * Register style directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveStyle($expression)
    {
        if (! empty($expression)) {
            $expression = $this->stripQuotes($expression);

            return "<link rel=\"stylesheet\" href=\"{$expression}\">";
        }

        return '<style>';
    }

    /**
     * Register endstyle directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndstyle($expression)
    {
        return '</style>';
    }

    /**
     * Register mix directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveMix($expression)
    {
        if (Str::endsWith($expression, ".css'")) {
            return "<link rel=\"stylesheet\" href=\"<?php echo mix({$expression}) ?>\">";
        }

        if (Str::endsWith($expression, ".js'")) {
            return "<script src=\"<?php echo mix({$expression}) ?>\"></script>";
        }

        return "<?php echo mix({$expression}); ?>";
    }

    /**
     * Register isnotnull directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveIsnotnull($expression)
    {
        return "<?php if (! is_null({$expression})) : ?>";
    }

    /**
     * Register endisnotnull directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndisnotnull($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register isnull directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveIsnull($expression)
    {
        return "<?php if (is_null({$expression})) : ?>";
    }

    /**
     * Register endisnull directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndisnull($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register isfalse directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveIsfalse($expression)
    {
        if (str_contains($expression, ',')) {
            $expression = $this->parseMultipleArgs($expression);

            return  "<?php if (isset({$expression->get(0)}) && (bool) {$expression->get(0)} === false) : ?>".
                "<?php echo {$expression->get(1)}; ?>".
                '<?php endif; ?>';
        }

        return "<?php if (isset({$expression}) && (bool) {$expression} === false) : ?>";
    }

    /**
     * Register endisfalse directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndisfalse($expression)
    {
        return '<?php endif; ?>';
    }

    /**
     * Register istrue directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveIstrue($expression)
    {
        if (str_contains($expression, ',')) {
            $expression = $this->parseMultipleArgs($expression);

            return  "<?php if (isset({$expression->get(0)}) && (bool) {$expression->get(0)} === true) : ?>".
                "<?php echo {$expression->get(1)}; ?>" . '<?php endif; ?>';
        }

        return "<?php if (isset({$expression}) && (bool) {$expression} === true) : ?>";
    }

    /**
     * Register endistrue directive.
     *
     * @param  $expression
     * @return string
     */
    private function directiveEndistrue($expression)
    {
        return '<?php endif; ?>';
    }
}
